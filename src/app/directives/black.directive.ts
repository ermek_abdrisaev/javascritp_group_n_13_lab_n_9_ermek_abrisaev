import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[appBlack]'
})

export class BlackDirective implements OnInit{
  constructor(private el: ElementRef){}

  ngOnInit(){
    this.el.nativeElement.style.background = 'black';
    this.el.nativeElement.style.color = 'white';
  }

}
