import { Directive, ElementRef, HostListener, Input, OnInit, Output, Renderer2 } from '@angular/core';
import { RouletteService } from '../shared/roulette.service';

@Directive({
  selector: '[appColor]'
})

export class ColorDirective implements OnInit  {
  @Input() number!: Number[];


  // @Input() set appColor(colorClass: string){
  //     this.renderer.removeClass(this.el.nativeElement, 'white');
  //     this.renderer.addClass(this.el.nativeElement, 'color');
  //
  // };
  //
  // colorClass = 'color';

  constructor(private el: ElementRef, private renderer: Renderer2, public rouletService: RouletteService){}

  ngOnInit(){
    this.el.nativeElement.style.background = 'red';
    this.el.nativeElement.style.background = 'black';
    this.el.nativeElement.style.background = 'green';
    this.el.nativeElement.style.color = 'white';
  }

}
