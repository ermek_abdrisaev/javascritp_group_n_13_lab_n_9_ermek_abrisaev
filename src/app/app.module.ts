import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { RouletteService } from './shared/roulette.service';
import { RedDirective } from './directives/red.directive';
import { BlackDirective } from './directives/black.directive';
import { ColorDirective } from './directives/color.directive';

@NgModule({
  declarations: [
    AppComponent,
    RedDirective,
    BlackDirective,
    ColorDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [RouletteService, ColorDirective],
  bootstrap: [AppComponent]
})
export class AppModule { }
