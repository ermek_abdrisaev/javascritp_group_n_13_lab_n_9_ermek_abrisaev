import { Component, ElementRef, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { RouletteService } from './shared/roulette.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  @ViewChild('betField') betField!: ElementRef;
  @ViewChild('redButton') redButton!: ElementRef;
  @ViewChild('blackButton') blackButton!: ElementRef;
  @ViewChild('zeroButton') zeroButton!: ElementRef;

  numbers: Number[] = [];
  inlineRadioOptions = '';

  constructor(public rouletteService: RouletteService){}

  ngOnInit(){
    this.rouletteService.newNumber.subscribe((numbers: Number[]) => {
      this.numbers = numbers;
    });
  }

  start(){
    this.rouletteService.start();
  }

  stop(){
    this.rouletteService.stop();
  }

  reset(){
    this.rouletteService.reset();
  }

  showBalance(){
    this.rouletteService.showBalance();
  }
}
