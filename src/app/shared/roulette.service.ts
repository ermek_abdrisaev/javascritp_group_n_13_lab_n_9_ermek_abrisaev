import { EventEmitter } from '@angular/core';

export class RouletteService {
  newNumber = new EventEmitter<Number[]>();

  private numbers: Number[] = [];
  private interval = 0;
  public balance: number = 100;

  generateNumber(){
    const index = Math.floor(Math.random() * 36);
    return index;
  };

  start(){
    this.interval = setInterval(() =>{
      this.numbers.push(this.generateNumber());
      this.newNumber.emit(this.numbers);
    }, 1000);
  };

  stop(){
    if(this.interval){
      clearInterval(this.interval);
    }
  };

  reset(){
    if(this.interval){
      this.numbers.splice(this.newNumber.length);
    }
  };

  getColor(number: Number){
    if((((number >= 1 && number <= 10) ||
      (number >= 19 && number <= 28)) && <any>number % 2 == 1) ||
      (((number >= 11 && number <= 18) ||
        (number >= 29 && number <= 36)) && <any>number % 2 == 0))
    {
      return "red";
    }else if(number == 0) {
      return "green";
    }else{
      return "black";
    }
  };

 showBalance(){
    return this.balance;
  }
}
